#pragma once
#include "Tool.h"
class Pawn :
	public Tool
{
	int _turn;
public:
	virtual bool isPossibleMove(Point src, Point dest , Board *b);
	virtual int move(Point src, Point dest, Board* b, bool MovOrNot);
	Pawn(char type);
	virtual ~Pawn();
};

