#pragma once
#include "Point.h"
#include "Board.h"

class Player
{
private:
	static bool _isWhiteTurn;
public:
	//Checks if a point in the board have a tool of the current player.
	static bool isItCurrPlayerTool(Point p, Board* b); //Know about who the turn is by the static member below.
	static bool isWhiteTurn();
	static void setBoolWhiteTurn(bool isItWhiteTurn);
	static bool hasChessOnCurrKing(Point currPlayerKingPoint, Board* b); //Checks if has chess on the current player's king.
	static bool hasChessOnRivalKing(Point rivalKingPoint, Board* b); //Checks if has chess on the rival player's king.
	//static bool isItCheckmateMove(Point rivalKingPoint, Board* b); //If the current player did a checkmate as a result of a tool's move to the other player.
	static Point findRivalKingPoint(Board* b)	;//Gives the point of the king that is not of the current player.
	static Point findCurrPlayerKingPoint(Board* b)	;//Gives the point of the current player's king.
	static void moveToolToDest(Point toolPoint, Point dest, Board* b); //Just moving  a tool to the dest - without any checks.
	Player();
	~Player();
};


