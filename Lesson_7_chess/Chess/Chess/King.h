#pragma once
#include "Point.h"
#include "Tool.h"
#include "Player.h"
#include "Math.h"



class King :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b, bool MovOrNot);
	King(char type);
	virtual ~King();
	virtual bool isPossibleMove(Point src, Point dest, Board* b);
	
private:
	bool wasThereChessMove(Point toolPoint, Point kingPoint, Board* b);
	void updateKingPointInBoard(Point newKingPoint, bool isWhiteKing, Board* b); //Updates a king point in the board.
};

