#pragma once
#include "Tool.h"
#include "Point.h"

class Knight :
	public Tool
{
public:
	virtual bool isPossibleMove(Point src, Point dest , Board *b);
	virtual int move(Point src, Point dest, Board* b, bool MovOrNot) ;
	Knight(char type);
	virtual ~Knight();
};

