#include "Queen.h"
#include "Player.h"
#include "Status Codes.h"








bool Queen::isPossibleMove(Point src, Point dest, Board* b)
{
	bool isPossible = isValidPath(src, dest) && !hasSomeToolInQueenPath(src, dest, b);
	return isPossible;
}

int Queen::move(Point src, Point dest, Board * b, bool MovOrNot)
{
	int statusCode = 0;
	Tool*** board = b->getBoard();


	if (this->isPossibleMove(src, dest, b))
	{

		b->setGoBackTool(board[dest.getX()][dest.getY()]);
		Player::moveToolToDest(src, dest, b);

		if (Player::hasChessOnCurrKing(Player::findCurrPlayerKingPoint(b), b)) //If has a chess on the current player after the moving.
		{
			//Returns the king back and the dest of the king.
			Player::moveToolToDest(dest, src, b);
			board[dest.getX()][dest.getY()] = b->getGoBackTool();

			return SELF_CHESS_MOVE;
		}
		/*else if (Player::isItCheckmateMove(Player::findRivalKingPoint(b), b))
			statusCode = CHESSMATE_MOVE;*/
		else if (this->isPossibleMove(dest, Player::findRivalKingPoint(b), b)) //If the move caused to a chess on the rival player.
			statusCode = CHESS_MOVE;
		else //If the move caused to a regular move - not something special.
			statusCode = VALID_MOVE;
	}
	else
		statusCode = INVALID_TOOL_MOVE;

	return statusCode;
	/*int type = src.getX() - src.getY() == dest.getX() - src.getY() ? 1 : src.getX() + src.getY() == dest.getX() + dest.getY() ? 2 : src.getX() == dest.getX() ? 3 : src.getY() == dest.getY() ? 4 : 0;
	switch (type)
	{
	case 1:
	{
		for (int i = src.getX() < dest.getX() ? src.getX() + 1 : dest.getX() + 1 ; i < (src.getX() > dest.getX() ? src.getX() - 1 : dest.getX() - 1); i++)
		{
			if (s[i][src.getY() - src.getY() + i] != nullptr) return 6;
		}
	}
	break;
	case 2:
	{
		for (int i = src.getX() < dest.getX() ? src.getX() + 1 : dest.getX() + 1; i < (src.getX() > dest.getX() ? src.getX() - 1 : dest.getX() - 1); i++)
		{
			if (s[i][src.getY() + src.getY() - i] != nullptr) return 6;
		}
	}
	break;
	case 3:
	{
		for (int i = src.getY() < dest.getY() ? src.getY() + 1 : dest.getY() + 1; i < (src.getY() > dest.getY() ? src.getY() - 1 : dest.getY() - 1); i++)
		{
			if (s[src.getX()][i] != nullptr) return 6;
		}
	}
	break;
	case 4:
	{
		for (int i = src.getX() < dest.getX() ? src.getX() + 1 : dest.getX() + 1; i < (src.getX() > dest.getX() ? src.getX() - 1 : dest.getX() - 1); i++)
		{
			if (s[i][src.getY()] != nullptr) return 6;
		}
	}
	break;
	default:
		return 6;
		break;
	}
	s[dest.getX()][dest.getY()] = s[src.getX()][src.getY()];
	s[src.getX()][src.getY()] = nullptr;
	return 0;*/
}

Queen::Queen(char type) : Tool(type)
{
}

Queen::~Queen()
{
}

bool Queen::isValidPath(Point src, Point dest)
{
	bool isValidBishopPath = abs(src.getX() - dest.getX()) == abs(src.getY() - dest.getY());
	bool isValidRookPath = ((src.getX() == dest.getX()) && src.getY() != dest.getY()) || ((src.getX() != dest.getX()) && (src.getY() == dest.getY()));
	return isValidBishopPath || isValidRookPath;
}

bool Queen::hasSomeToolInQueenPath(Point src, Point dest, Board * b)
{
	bool has = false;
	bool isSlantCheck = abs(src.getX() - dest.getX()) == abs(src.getY() - dest.getY());//If the path is in a slant path - so we need to check it in a slant way.
	bool isVerticalCheck = src.getX() != dest.getX(); //If to check the path in a vertical way or in a horizontal way.
	Tool*** board = b->getBoard();

	if (isSlantCheck)
	{
		int x = src.getX(), y = src.getY(); //Of the board place that we will check.
		int xFactor = 0, yFactor = 0; //The amount of changing the x and y in each iteration.

		//Inits the factors:
		this->initXAndYFactors(src, dest, &xFactor, &yFactor);

		//We begin to check from the first diagonal place, not from the src itself.
		x += xFactor;
		y += yFactor;

		while ((x != dest.getX() && y != dest.getY()) && !has)//Runs until we find a some tool in the path or we've got to the dest.
		{
			if (board[x][y] != nullptr)
				has = true;

			x += xFactor;
			y += yFactor;
		}
	}
	else
	{
		if (isVerticalCheck)
		{
			int xFactor = src.getX() > dest.getX() ? -1 : 1;
			for (Point curr(src.getX() + xFactor, src.getY()); curr != dest; curr.setX(curr.getX() + xFactor))
			{
				if (board[curr.getX()][curr.getY()] != nullptr) //If has some tool in the path.
				{
					has = true;
					break;
				}
					
			}
		}
		else
		{
			int yFactor = src.getY() > dest.getY() ? -1 : 1;
			for (Point curr(src.getX(), src.getY() + yFactor); curr != dest; curr.setY(curr.getY() + yFactor))
			{
				if (board[curr.getX()][curr.getY()] != nullptr) //If has some tool in the path.
				{
					has = true;
					break;
				}

			}
		}
	}

	return has;
}

void Queen::initXAndYFactors(Point src, Point dest, int * xFactor, int * yFactor)
{
	//Inits the factors:
	if (dest > src)
	{
		*xFactor = 1;
		*yFactor = 1;
	}
	else if (dest < src)
	{
		*xFactor = -1;
		*yFactor = -1;
	}
	else if (dest.avgPoint() == src.avgPoint())
	{
		if (dest.getX() > src.getX())
		{
			*xFactor = 1;
			*yFactor = -1;
		}
		else
		{
			*xFactor = -1;
			*yFactor = 1;
		}
	}

}
