#include "Pipe.h"
#include "Tool.h"
#include <iostream>
#include <thread>
#include <string>
#include "Point.h"
#include "Board.h"
#include "Bishop.h"
#include "Player.h"
#include <stdlib.h>
#include <stdio.h>

using std::cout;
using std::endl;
using namespace std;


int main()
{


	Board b(INIT_BOARD_MSG);
	Tool* currentTool = nullptr;
	string send = "0";
	
	
	//getchar();


	srand(time_t(NULL));
	Pipe p;


	bool isConnect = p.connect();
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return 0;
		}
	}


	// Connection is astablished ... 

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE
	strcpy_s(msgToGraphics, INIT_BOARD_MSG); // just example...

	p.sendMessageToGraphics(msgToGraphics);   // send the board string

											  // get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();
	
	while (msgFromGraphics != "quit")
	{
		Point src(msgFromGraphics[1], msgFromGraphics[0]);
		Point dest(msgFromGraphics[3], msgFromGraphics[2]);
		currentTool = b.getBoard()[src.getX()][src.getY()];

		cout << "Source: ";
		src.PrintPoint();
		cout << "Dest: ";
		dest.PrintPoint();

		if (!(b.isValidBoardPoint(src) && b.isValidBoardPoint(dest))) //If src and dest in board.
			send[0] = '5';
		else if (src == dest) //If the src and the dest are equal each other.
			send[0] = '7';
		else if (!Player::isItCurrPlayerTool(src, &b)) //If the src tool is not of the curr player 
			send[0] = '2';
		else if (Player::isItCurrPlayerTool(dest, &b)) //if the dest tool is of the curr player.
			send[0] = '3';
		else //Valid move.
			send[0] = (char)(currentTool->move(src, dest, &b, true) + 48);
		
		strcpy_s(msgToGraphics, send.c_str()); // msgToGraphics should contain the result of the operation

		p.sendMessageToGraphics(msgToGraphics);

		b.printBoard();

		if (send == "0" || send == "1" || send == "8") //If the moves has done.
			Player::setBoolWhiteTurn(!Player::isWhiteTurn()); //The turn is swapped

		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
	return 0;
}
