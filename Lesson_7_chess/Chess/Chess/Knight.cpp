#include "Knight.h"






bool Knight::isPossibleMove(Point src, Point dest , Board *b )
{
	int result = move(src, dest, b, false);
	return  (result == 0  || result == 8) ;
}

int Knight::move(Point src, Point dest, Board * b , bool MovOrNot)
{
	int currX = src.getX();
	int currY = src.getY();
	int destX = dest.getX();
	int destY = dest.getY();
	int gap = 0;
	Tool* temp = b->getBoard()[dest.getX()][dest.getY()];
	char type = this->getType();
	if (currX == destX && currY == destY)
	{
		return 7; // Same dest and curry
	}
	if (currX == destX || currY == destY)
	{
		return 6; // Same dest and curry
	}
	if (temp != nullptr && !(islower(temp->getType()) != islower(type)))
	{
		return 6;
	}

	// Check legal of action 


	if (currY < destY)
	{
		gap = (destY - currY);
		if (gap != 1 && gap != 2)
		{
			return 6;
		}
		cout << "Pass gap is << " << gap << "\n";
		switch (gap)
		{
		case 1:
			cout << "Gap 1: " << abs(currX - destX) << "\n";
			if (abs(currX - destX) !=2)
			{
				return 6;
			}
			break;
		case 2:
			cout << "Gap 2: " << abs(currX - destX) << "\n";

			if (abs(currX - destX) != 1)
			{
				return 6;
			}
			break;
		default:
			break;
		}
	}
	else // if(currY > destY)
	{
		gap = (currY -destY  );
		if (gap != 1 && gap != 2)
		{
			return 6;
		}
		cout << "Pass gap is << " << gap << "\n";
		switch (gap)
		{
		case 1:
			cout << "Gap 1: " << abs(currX - destX) << "\n";
			if (abs(currX - destX) != 2)
			{
				return 6;
			}
			break;
		case 2:
			cout << "Gap 2: " << abs(currX - destX) << "\n";

			if (abs(currX - destX) != 1)
			{
				return 6;
			}
			break;
		default:
			break;
		}
	}

	if (MovOrNot)
	{
		b->getBoard()[dest.getX()][dest.getY()] = b->getBoard()[src.getX()][src.getY()];
		b->getBoard()[src.getX()][src.getY()] = nullptr;
	}
	
	return 0;
}

Knight::Knight(char type) : Tool(type)
{
	
}




Knight::~Knight()
{
}
