#include "Rook.h"
#include <stdio.h>
#include <ctype.h>

bool Rook::isPossibleMove(Point src, Point dest, Board* b)
{
	int result = move(src, dest, b, false);
	return  (result == 0 || result == 8);
}

int Rook::move(Point src, Point dest, Board* b ,  bool MovOrNot)
{
	int currX = src.getX();
	int currY = src.getY();
	int destX = dest.getX();
	int destY = dest.getY();
	


	if (currX == destX && currY == destY)
	{
		return 7; // Same dest and curry
	}



	cout << "@ Checking Rook ----> " << this->getType() << endl;
	char type = this->getType();
	if (!((dest.getX() == src.getX()) || (dest.getY() == src.getY())))
	{
		cout << "@ Rook -> " << type << " <- :::Movement is not legal " << endl;
		return 6; // Movement is not legal
		
	}
	
	Tool* temp = b->getBoard()[dest.getX()][dest.getY()];
	if (temp != nullptr)
	{
		cout << "Tool found ----> " << temp->getType() << endl;
		if (islower(temp->getType()) != islower(type))  // Different type
		{
			if (anotherCheck(currX, currY, destX, destY, b) == 6)
			{
				return 6;
			}
			cout << "[*] Moved rook " << endl;
			if (MovOrNot)
			{
				b->getBoard()[dest.getX()][dest.getY()] = b->getBoard()[src.getX()][src.getY()];
				b->getBoard()[src.getX()][src.getY()] = nullptr;
			}
			return 0;
		}
		else
		{
			cout << "[*] Cant eat your own type" << endl;
			return 3;
		}
	}
	else
	{
		if (anotherCheck(currX, currY, destX, destY, b) == 6) 
		{
			return 6;
		}
		if (MovOrNot)
		{
			b->getBoard()[dest.getX()][dest.getY()] = b->getBoard()[src.getX()][src.getY()];
			b->getBoard()[src.getX()][src.getY()] = nullptr;
		}

		return 0;
	}

}

int Rook::anotherCheck(int currX, int currY, int destX, int destY, Board * b)
{
	cout << "[*] No tool , checking midway ..." << endl;
	if (destX == currX) // Same row 
	{
		cout << "[*] Same row check " << endl;
		if (destY < currY)
		{
			for (int y = destY + 1; y < currY; y++)
			{
				if (b->getBoard()[destX][y] != nullptr)
				{
					return 	6; // There is another tool in the way
				}
			}
		}
		else
		{
			for (int y = currY + 1; y < destY; y++)
			{
				if (b->getBoard()[destX][y] != nullptr)
				{
					return 	6; // There is another tool in the way
				}
			}
		}
	}
	else
	{
		if (destY == currY)
		{
			cout << "[*] same coulmn" << endl;
			if (destX > currX)
			{
				for (int i = currX + 1; i < destX; i++)
				{
					if (b->getBoard()[i][currY] != nullptr)
					{
						cout << "[!] Great " << i << " " << currY << endl;
						return 6;
					}
				}
			}
		}
		else
		{
			if (destX < currX)
			{
				for (int i = destX + 1; i < currX; i++)
				{
					if (b->getBoard()[i][currY] != nullptr)
					{
						cout << "[!] Not " << i << " " << currY << endl;
						return 6;
					}
				}
			}

		}
	}
	return 0;
}

Rook::Rook(char type) : Tool(type)
{
}

Rook::~Rook()
{
}
