#include "Bishop.h" 
#include "Player.h"
#include "Math.h"





int Bishop::move(Point src, Point dest, Board* b, bool MovOrNot)
{
	int statusCode = 0;
	Tool*** board = b->getBoard();

	
	if (this->isPossibleMove(src, dest, b))
	{

		b->setGoBackTool(board[dest.getX()][dest.getY()]);
		Player::moveToolToDest(src, dest, b);

		if (Player::hasChessOnCurrKing(Player::findCurrPlayerKingPoint(b), b)) //If has a chess on the current player after the moving.
		{
			//Returns the king back and the dest of the king.
			Player::moveToolToDest(dest, src, b);
			board[dest.getX()][dest.getY()] = b->getGoBackTool();

			return SELF_CHESS_MOVE;
		}
		/*else if (Player::isItCheckmateMove(Player::findRivalKingPoint(b), b))
			statusCode = CHESSMATE_MOVE;*/
		else if (this->wasThereChessMove(dest, Player::findRivalKingPoint(b), b))
			statusCode = CHESS_MOVE;
		else //If the move caused to a regular move - not something special.
			statusCode = VALID_MOVE;
	}
	else
		statusCode = INVALID_TOOL_MOVE;

	return statusCode;
}

bool Bishop::isPossibleMove(Point src, Point dest, Board* b)
{
	bool isPossibleMov = abs(src.getX() - dest.getX()) == abs(src.getY() - dest.getY());
	bool hasSomeToolInPath = this->HasAToolInPath(src, dest, b);
	return isPossibleMov && !hasSomeToolInPath;
}

bool Bishop::wasThereChessMove(Point toolPoint, Point kingPoint, Board * b)
{
	bool wasThere = false;
	if (this->isPossibleMove(toolPoint, kingPoint, b))
		wasThere = true;

	return wasThere;
}

bool Bishop::HasAToolInPath(Point src, Point dest, Board* b)
{
	Tool*** board = b->getBoard();
	bool has = false;
	int x = src.getX(), y = src.getY(); //Of the board place that we will check.
	int xFactor = 0, yFactor = 0; //The amount of changing the x and y in each iteration.

	//Inits the factors:
	this->initXAndYFactors(src, dest, &xFactor, &yFactor);

	//We begin to check from the first diagonal place, not from the src itself.
	x += xFactor;
	y += yFactor;
	
	while ((x != dest.getX() && y != dest.getY()) && !has)//Runs until we find a some tool in the path or we've got to the dest.
	{
		if (board[x][y] != nullptr)
			has = true;

		x += xFactor;
		y += yFactor;
	}
	
	return has;
}

void Bishop::initXAndYFactors(Point src, Point dest, int * xFactor, int * yFactor)
{
	//Inits the factors:
	if (dest > src)
	{
		*xFactor = 1;
		*yFactor = 1;
	}
	else if (dest < src)
	{
		*xFactor = -1;
		*yFactor = -1;
	}
	else if (dest.avgPoint() == src.avgPoint())
	{
		if (dest.getX() > src.getX())
		{
			*xFactor = 1;
			*yFactor = -1;
		}
		else
		{
			*xFactor = -1;
			*yFactor = 1;
		}
	}

}

Bishop::Bishop(char type) : Tool(type)
{

}



Bishop::~Bishop()
{
}


