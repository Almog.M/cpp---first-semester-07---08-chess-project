#include "Pawn.h"
#include <math.h>





bool Pawn::isPossibleMove(Point src, Point dest , Board *b)
{
	int result = move(src, dest, b, false);
	return  (result == 0 || result == 8 || result == 1);
}

int Pawn::move(Point src, Point dest, Board * b, bool MovOrNot)
{
	int currX = src.getX();
	int currY = src.getY();
	int destX = dest.getX();
	int destY = dest.getY();
	// if mat ....
	if (currX == destX && currY == destY)
	{
		return 7; // Same dest and curry
	}

	cout << "@ Checking Pawn ----> " << this->getType() << endl;
	char type = this->getType();
	Tool* temp = b->getBoard()[dest.getX()][dest.getY()]; // Tool to check

	if (destY != currY ) // Player might want to attack
	{
		if (temp == nullptr)
		{
			cout << "[*] There is nothing to attack" << endl;
			return 6;
		}
		if (abs(destY - currY) >1) // Max movement
		{
			return 6; // Illegal move
		}
		if (islower(temp->getType()) != islower(type)) // Different type dest
		{
			//if(chess) { return chess result}
			if (MovOrNot)
			{
				b->getBoard()[dest.getX()][dest.getY()] = b->getBoard()[src.getX()][src.getY()];
				b->getBoard()[src.getX()][src.getY()] = nullptr;
			}

			return 0; // Ok
		}
		else
		{
			cout << "[*] Cant eat your own tool" << endl; 
		}
	}
	if (b->getBoard()[dest.getX()][dest.getY()] != nullptr)
	{
		return 6; // Player is there
	}
	if (islower(type) && destX > currX  || !islower(type) && destX < currX) // if black pawn tries to go back
	{
		cout << "[*] Pawn cant go back" << endl;
		return 6;
	}
	
	if (_turn == 0)
	{
		if (abs(destX - currX) >2)
		{
			cout << "[*] Cant go over 2 steps";
			return 6;
		}	
		_turn++;
	}
	else
	{
		if (abs(destX - currX) >1)
		{
			cout << "[*] Cant go over 1 steps";
			return 6;
		}
	}
	if (MovOrNot)
	{
		b->getBoard()[dest.getX()][dest.getY()] = b->getBoard()[src.getX()][src.getY()];
		b->getBoard()[src.getX()][src.getY()] = nullptr;
	}
	
	
	return 0;

}

Pawn::Pawn(char type) : Tool(type)
{
	_turn = 0;
}

Pawn::~Pawn()
{
}
