#pragma once
#include "Tool.h"
class Queen :
	public Tool
{
public:
	virtual bool isPossibleMove(Point src, Point dest, Board* b);
	virtual int move(Point src, Point dest, Board* b, bool MovOrNot);
	Queen(char type);
	virtual ~Queen();
private:
	bool isValidPath(Point src, Point dest); //If the path is a queen path, ignore from the tools in the path.
	bool hasSomeToolInQueenPath(Point src, Point dest, Board* b); //Checks if has a tool in a queen path.
	void initXAndYFactors(Point src, Point dest, int * xFactor, int * yFactor);
};

