#include "Player.h"
#include "Board.h"
#include "Tool.h"

bool Player::_isWhiteTurn = strlen(INIT_BOARD_MSG) == 65 && INIT_BOARD_MSG[64] == '0'; //If the msg is in a valid len and the last char is a zero.

bool Player::isItCurrPlayerTool(Point p, Board * b)
{
	Tool* toolInPoint = b->getBoard()[p.getX()][p.getY()];
	bool isWhitePlayerAndWhiteTool = toolInPoint && Player::_isWhiteTurn && !toolInPoint->isBlackCheck(); //If this is the white turn and the tool is with a big letter symbol.
	bool isBlackPlayerAndBlackTool = toolInPoint && !Player::_isWhiteTurn && toolInPoint->isBlackCheck(); //If this is the black turn and the tool is with a small letter symbol.
	return isWhitePlayerAndWhiteTool || isBlackPlayerAndBlackTool;
}

bool Player::isWhiteTurn()
{
	return Player::_isWhiteTurn;
}

void Player::setBoolWhiteTurn(bool isItWhiteTurn)
{
	Player::_isWhiteTurn = isItWhiteTurn;
}

bool Player::hasChessOnCurrKing(Point currPlayerKingPoint, Board * b)
{
	Tool*** board = b->getBoard();
	bool has = false; //About has or not has chess on current king.
	Tool* currTool = nullptr;

	//Runs on the board.
	for (int i = 0; i <= b->getMaxRowIndex() && !has; i++)
	{
		for (int j = 0; j <= b->getMaxColIndex() && !has; j++)
		{
			if (!has && board[i][j] && !Player::isItCurrPlayerTool(Point(i, j), b)) //If the tool is the tool of the another player for sure.
			{
				currTool = board[i][j];
				//if the current player king is on chess.
				if (currTool->isPossibleMove(Point(i, j), currPlayerKingPoint, b))
				{
					has = true;
				}
			}
		}
	}
	return has;
}

bool Player::hasChessOnRivalKing(Point rivalKingPoint, Board * b)
{
	Tool*** board = b->getBoard();
	bool has = false; //About has or not has chess on current king.
	Tool* currTool = nullptr;

	//Runs on the board.
	for (int i = 0; i <= b->getMaxRowIndex() && !has; i++)
	{
		for (int j = 0; j <= b->getMaxColIndex() && !has; j++)
		{
			if (!has && board[i][j] && Player::isItCurrPlayerTool(Point(i, j), b)) //If the tool is the tool of the another player for sure.
			{
				currTool = board[i][j];
				//if the rival player's king is on chess.
				if (currTool->isPossibleMove(Point(i, j), rivalKingPoint, b))
				{
					has = true;
				}
			}
		}
	}
	return has;
}

//bool Player::isItCheckmateMove(Point rivalKingPoint, Board * b)
//{
//	int i = 0;
//	Point currPoint = Point(-1,-1);
//	Tool*** board = b->getBoard();
//	bool hasCheckmate = false;
//	Dynamic memory.
//	Point* validPointsToTryToMove = Player::findValidKingPointsToTryToMove(rivalKingPoint, b); //Finds the points that the rival king might move - Point with (-1, -1) coordination at the end.
//
//	while(!hasCheckmate && validPointsToTryToMove[i] != Point(-1, -1))
//	{
//		currPoint = validPointsToTryToMove[i];
//		b->setGoBackTool(board[currPoint.getX()][currPoint.getY()]);
//		Player::moveToolToDest(rivalKingPoint, currPoint, b);
//		
//
//		
//		i++;
//	}
//
//
//	delete[] validPointsToTryToMove;
//	return hasCheckmate;
//}


Point Player::findRivalKingPoint(Board * b)
{
	Point rivalKingPoint = Point(-1,-1);//Default.
	
	if (Player::isWhiteTurn())
		rivalKingPoint = b->getBlackKingPoint();
	else
		rivalKingPoint = b->getWhiteKingPoint();

	return rivalKingPoint;
}

Point Player::findCurrPlayerKingPoint(Board * b)
{
	
	Point currPlayerKingPoint = Point(-1, -1);//Default.

	if (Player::isWhiteTurn())
		currPlayerKingPoint = b->getWhiteKingPoint();
	else
		currPlayerKingPoint = b->getBlackKingPoint();

	return currPlayerKingPoint;
	
}

void Player::moveToolToDest(Point toolPoint, Point dest, Board * b)
{
	Tool*** board = b->getBoard();
	board[dest.getX()][dest.getY()] = board[toolPoint.getX()][toolPoint.getY()];
	board[toolPoint.getX()][toolPoint.getY()] = nullptr; //The old place is empty now.
}

Player::Player()
{
}

Player::~Player()
{
}
