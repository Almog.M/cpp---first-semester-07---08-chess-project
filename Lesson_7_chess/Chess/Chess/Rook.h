#pragma once
#include "Tool.h"
#include "Point.h"

class Rook :
	public Tool
{
public:
	virtual bool isPossibleMove(Point src, Point dest , Board* b);
	virtual int move(Point src, Point dest, Board* b, bool MovOrNot);
	int anotherCheck(int currX, int currY, int destX, int destY, Board* b);
	Rook(char type);
	virtual ~Rook();
};

