#pragma once
#include <string>
#include "Point.h"
#include "Board.h"
#include "Status Codes.h"

class Board;

using namespace std;
class Tool
{
protected:
	 char  _type;
public:
	// Getters
	char getType() const;


	bool isBlackCheck() const;
	// Setters

	//Trying to move a player and returns the code of the action. 
	//The 'moveOrNot' flag is for say if you want actually move the tool to the dest
	//or just checking if you can move the tool.
	//True for actually move, false for just checking if can or not.
	virtual int move(Point src , Point dest, Board* b, bool MovOrNot) = 0;

	//Checks if  the tool can move according to the tool path and tools in this path.
	//Ignore from valid src, dest!!!!
	//Assuming that the src and the dest are valid!!!
	virtual bool isPossibleMove(Point src, Point dest, Board* b) = 0; 

	//C and D
	Tool(char type);
	virtual ~Tool();
}; 
