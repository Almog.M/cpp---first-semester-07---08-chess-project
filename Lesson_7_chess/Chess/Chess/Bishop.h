#pragma once
#pragma once
#include "Tool.h"
#include "Point.h"

class Bishop :
	public Tool
{
public:
	Bishop(char type);
	virtual ~Bishop();
	virtual int move(Point src, Point dest, Board* b, bool MovOrNot);
	virtual bool isPossibleMove(Point src, Point dest, Board* b);

private:
	bool wasThereChessMove(Point toolPoint, Point kingPoint, Board* b);
	bool HasAToolInPath(Point src, Point dest, Board* b); //Checks if has a tool between the points according to the tool move.
	void initXAndYFactors(Point src, Point dest, int* xFactor, int* yFactor); //This function helps we about the checking if we have some tool in the src and dst path.
};
