#include "King.h"
#include "Player.h"





int King::move(Point src, Point dest, Board* b, bool MovOrNot)
{
	int statusCode = 0;
	Tool*** board = b->getBoard();
	
	
	if (this->isPossibleMove(src, dest, b)) //If the king can move to the place.
	{
		b->setGoBackTool(board[dest.getX()][dest.getY()]);
		Player::moveToolToDest(src, dest, b);
		

		if (Player::hasChessOnCurrKing(Player::findCurrPlayerKingPoint(b), b)) //If has a chess on the current player after the moving.
		{
			//Returns the king back and the dest of the king.
			Player::moveToolToDest(dest, src, b);
			board[dest.getX()][dest.getY()] = b->getGoBackTool();

			return SELF_CHESS_MOVE;
		}
		/*else if (Player::isItCheckmateMove(Player::findRivalKingPoint(b), b))
			statusCode = CHESSMATE_MOVE;*/
		else if (this->wasThereChessMove(dest, Player::findRivalKingPoint(b), b))
			statusCode = CHESS_MOVE;
		else //If the move caused to a regular move - not something special.
			statusCode = VALID_MOVE;
		this->updateKingPointInBoard(src, Player::isWhiteTurn(), b);
		
	}
	else
		statusCode = INVALID_TOOL_MOVE;

	return statusCode;
}

King::King(char type) : Tool(type)
{

}

King::~King()
{
}

bool King::isPossibleMove(Point src, Point dest, Board* b)
{
	//If we are moving one place to right\left or up\down or mix of them.
	return (abs(src.getX() - dest.getX()) == 1 || abs(src.getY() - dest.getY()) == 1) && !Player::isItCurrPlayerTool(dest, b);
}

bool King::wasThereChessMove(Point toolPoint, Point kingPoint, Board * b)
{
	bool wasThere = false;
	if (this->isPossibleMove(toolPoint, kingPoint, b))
		wasThere = true;

	return wasThere;
}


void King::updateKingPointInBoard(Point newKingPoint, bool isWhiteKing, Board* b)
{
	if (isWhiteKing)
		b->setWhiteKingPoint(newKingPoint);
	else //If the black king has moved.
		b->setBlackKingPoint(newKingPoint);
}

